package com.raulpedraza.tweetlocator_androidavanzado.dao;

/**
 * Created by raulpedraza on 15/5/16.
 */
public interface Persistable {
    void setId(long id);
}
